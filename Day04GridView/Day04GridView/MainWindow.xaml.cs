﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04GridView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        List<Person> PeopleList = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
            lstPeople.DataContext = PeopleList;
        }
        class Person
        {
            //constructor
            public Person(string name, int age)
            {
                Name = name;
                Age = age;
            }


            //Setters&getters
            private string _name;
            public string Name
            {
                set
                {
                    if (value.Equals(""))
                    {
                        Console.WriteLine("Name field should not be empty");
                    }
                    _name = value;
                }
                get
                {
                    return _name;
                }
            }
            //
            private int _age;
            public int Age
            {
                set
                {
                    if (value < 0)
                    {
                        Console.WriteLine("age should be greater than zero");
                    }
                    _age = value;
                }
                get
                {
                    return _age;
                }
            }
            public override string ToString()
            {
                return Name + "," + Age + " years old";

            }


        }

        Person selectederson;

        private void LstPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectederson = (Person)lstPeople.SelectedItem;
            if (selectederson != null)
            {
                tbName.Text = selectederson.Name;
                tbAge.Text = selectederson.Age + "";

            }
            else
            {
                //clean upor not
            }
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            int age = int.Parse(tbAge.Text);
            Person p = new Person(name, age);
            //PeopleList.Add(p);
            //lstPeople.ItemsSource = PeopleList;
            lstPeople.Items.Add(p);
            tbName.Text = "";
            tbAge.Text = null;
        }

        private void BtUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (selectederson == null)
            {
                return;
            }
            string name = tbName.Text;
            int age;
            if (!int.TryParse(tbAge.Text, out age))
            {
                MessageBox.Show(this, "Select an age", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            selectederson.Name = name;
            selectederson.Age = age;
            lstPeople.Items.Refresh();

        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            if (selectederson == null)
            {
                return;
            }

            lstPeople.Items.Remove(selectederson);

        }
    }
}
