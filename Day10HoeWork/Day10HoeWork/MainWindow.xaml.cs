﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10HoeWork
{
   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Todo selectedTask;
        public MainWindow()
        {
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum));
            refreshList();
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            TodoDbContext ctx = new TodoDbContext();
            //Equivilant to insert
            Todo addTask = new Todo { Task=tbTask.Text,Difficulty =(int) sliderDifficulty.Value, DueDate = dpDueDate.SelectedDate,Status=(Todo.StatusEnum)comboStatus.SelectedItem};//state:new, detached
            ctx.Task.Add(addTask);//Insert operation is scheduled here but Not executed yet
           ctx.SaveChanges();
            Console.WriteLine("Record Added");
            refreshList();
        }
        public void refreshList() {
            //EMPTY FIELDS
            btEdit.IsEnabled = false ;
            btDelete.IsEnabled = false;
            tbTask.Text = "";
            comboStatus.SelectedIndex = 0;
            sliderDifficulty.Value = 2;
            Console.WriteLine("refresh process");
            TodoDbContext ctx = new TodoDbContext();
            List<Todo> TasksList = new List<Todo>();
            var items = (from p in ctx.Task select p).ToList();
            foreach (var item in items)
            {
                TasksList.Add(item);
            }
            lvTasks.ItemsSource = TasksList;
        }

        private void BtEdit_Click(object sender, RoutedEventArgs e)
        {
            TodoDbContext ctx = new TodoDbContext();
            Todo editTask= (from p in ctx.Task where p.Id ==selectedTask.Id select p).FirstOrDefault<Todo>();
            if (editTask != null)
            {
                editTask.Task = tbTask.Text;
                editTask.Status= (Todo.StatusEnum)comboStatus.SelectedItem;
                editTask.DueDate = dpDueDate.SelectedDate;
                editTask.Difficulty =(int) sliderDifficulty.Value;// entity framework is watching and notices the modification
                ctx.SaveChanges(); // update the database to synchronize entities in memory with the database
               
                refreshList(); 
            }
            else
            {
                Console.WriteLine("record to update not found");
            }
        }

        private void LvTasks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Todo selectedTask = (Todo)lvTasks.SelectedValue;
            if (selectedTask == null)
            {
                return;
            }
            
            tbId.Text = selectedTask.Id +"";
            tbTask.Text = selectedTask.Task;
            sliderDifficulty.Value = (selectedTask.Difficulty);
            comboStatus.SelectedValue = selectedTask.Status;
            btEdit.IsEnabled = true;
            btDelete.IsEnabled = true;
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            TodoDbContext ctx = new TodoDbContext();
            Todo deleteTask = (from p in ctx.Task where p.Id ==selectedTask.Id select p).FirstOrDefault<Todo>();
            if (deleteTask != null)
            {
                ctx.Task.Remove(deleteTask);
                ctx.SaveChanges();
                Console.WriteLine("Record Deleted");
                refreshList();

            }
            else
            {
                return;
        }
    }

        private void BtExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
              
                {

                    TodoDbContext ctx = new TodoDbContext();
                    List<Todo> TasksList = new List<Todo>();
                    var items = (from p in ctx.Task select p).ToList();
                    foreach (var item in items)
                    {
                        TasksList.Add(item);
                    }
                    using (var writer = new StreamWriter(saveFileDialog.FileName))
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.WriteRecords(TasksList);
                        writer.Flush();
                    }

                }
            }
        
        }
    }
}

