﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Day10HoeWork
{
    class Todo
    {
        public enum StatusEnum { Done = 1, Postponded = 2, Cancelled = 3 }
        //[Key]
        public int Id { get; set; }
        //
        [Required]
        [StringLength(50)]
        public string Task { get; set; }
        [Required]
        public int Difficulty { get; set; }
        [Required]
        public DateTime? DueDate { get; set; }
        [Required]
        [EnumDataType(typeof(StatusEnum))]
        public StatusEnum Status { get; set; }
        //
        public override string ToString()
        {
            return String.Format("{0}:{1} by  {2} / difficulty is {3}, {4}",Id,Task,DueDate.Value.Date.ToShortDateString(), Difficulty,Status);
        }


    }
}
