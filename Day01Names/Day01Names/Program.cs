﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Names
{
    class Program
    {
        static List<string> peopleList = new List<string>();
       
        static void Main(string[] args)
        {
            
            while (true)
            {
                Console.WriteLine("Enter name");
                string name = Console.ReadLine();
                if (!name.Equals(""))
                {
                    peopleList.Add(name);

                }
                else
                {
                    Console.WriteLine("Names entered so far:\n");
                    foreach (string str in peopleList)
                    {
                        Console.WriteLine(str);
                    }
                    Console.WriteLine("press any key to continue\n");
                    Console.ReadKey();
                    break;
                }
            }
        }
    }
}
