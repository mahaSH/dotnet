﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05TravelList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        static List<Trip> TripList = new List<Trip>();
        class Trip
        {
            public Trip(string destination, string name, string passportNo, DateTime arrival, DateTime departure)
            {
                Destination = destination;
                name = name;
                TravellerPassprotNo = passportNo;
                DepartureDate = departure;
                ArrivalDate = arrival;

            }
            private string _destination;
            public string Destination
            {// 2-30 characters, no semicolons
                set
                {
                    if (value.Length < 2 || value.Length > 30 || value.Contains(';'))
                    {
                        throw new ArgumentException("destination should be 2-30 characters, no semicolons");
                    }
                    _destination = value;
                }
                get
                {
                    return _destination;
                }

            }
            private string _travellerName;
            public string TravellerName
            {// 2-30 characters, no semicolons
                set
                {
                    if (value.Length < 2 || value.Length > 30 || value.Contains(';'))
                    {
                        throw new ArgumentException("Traveller name should be 2-30 characters, no semicolons");
                    }
                    _travellerName = value;
                }
                get
                {
                    return _travellerName;
                }
            }
            private string _travellerPassportNo;
            public string TravellerPassprotNo
            {// two uppercase letters followed by 8 digts
                set
                {
                    Regex regexObj = new Regex(@"[A-Z]{2}\d{6}");
                    Match matchResult = regexObj.Match(value);
                    if (!matchResult.Success)

                    {
                        throw new ArgumentException("Traveller name should be 2-30 characters, no semicolons");
                    }
                    _travellerName = value;
                }
                get
                {
                    return _travellerPassportNo;
                }
            }
            private DateTime _departureDate;
            public DateTime DepartureDate;
            private DateTime _arrivalDate;
            public DateTime ArrivalDate;
            public void setDepartureAndReturnDates(DateTime depDate, DateTime retDate)
            {// year 1900-2100, departure before return
                if (depDate.Year > 2100 || depDate.Year < 1900)
                {
                    throw new ArgumentException("departure date must be between 1900 & 2100");
                }
                else if (retDate.Year > 2100 || retDate.Year < 1900)
                {
                    throw new ArgumentException("return date must be between 1900 & 2100");
                }
                else if (retDate.Date < depDate.Date)
                {
                    throw new ArgumentException("return date must be after departure date");
                }
                DepartureDate = depDate;
               ArrivalDate = retDate;
            }
            public override string ToString()
            {
                return string.Format("{0},with passoport No.{1}travelles to {2} in {3} and will be back in  {4}",TravellerName,TravellerPassprotNo,Destination,DepartureDate,ArrivalDate);
            }
        }
        static void SaveDataToFile()
        {
            try
            {
                List<String> linesList = new List<String>();
                foreach (Trip t in TripList)
                {
                    linesList.Add(String.Format("{0};{1};{2};{3};{4};", t.Destination,t.TravellerName,t.TravellerPassprotNo,t.DepartureDate,t.ArrivalDate));
                }
                File.WriteAllLines(@"..\..\Trips.txt", linesList); // ex
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }
        static void LoadDataFromFile()
        {
            Trip t;
            try
            {
                var dataFromFile = File.ReadAllLines(@"..\..\Trips.txt");
                string[] tokens = dataFromFile.ToArray();

                for (int i = 0; i < tokens.Length; ++i)
                {

                    string[] line = tokens[i].Split(';');
                    if (line.Length != 5)
                    {
                        Console.WriteLine("Invalid line:", i);
                        continue;
                    }
                    else
                        t = new Trip(line[0], line[1], line[2], line[3].toString("yyyyMMdd"), line[4]);
                    TripList.Add(t);
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }

}