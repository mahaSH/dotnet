﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{
    class BetterCity
    {
        public BetterCity(String name)
        {
            Name2 = name;
        }
        public string Name { get; set;}
        private string _name2;
        public string Name2
        {
            get
            {
                return _name2;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new InvalidOperationException("name's length should be more than two");

                }
                _name2 = value;
            }
        }
    }
    class City
    {
        public string Name;
        public double Population;
        public override string ToString()
        {
            return string.Format("City name:{0},with population of {1} million", Name, Population);
        }
    }
    class Program
    {
        static public  List<City> GlobalCitiesList = new List<City>();
        static void Main(string[] args)
        {
            BetterCity bc1 = new BetterCity { Name = "???" };
            bc1.Name = "Montreal";
            City c1 = new City { Name = "Montreal", Population = 3.4 };
            Console.WriteLine("c1->" + c1);
            List<City> citiesList = new List<City>();
            citiesList.Add(c1);
            //
            Console.WriteLine("prees any key to continue");
            Console.ReadKey();

        }
    }
}
