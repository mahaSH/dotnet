﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestCarsOwnerswpf
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        Owner currOwner;
        ParkContext ctx;
        public CarsDialog(Owner owner)
        {
            ctx = new ParkContext();
            
            currOwner = owner;
            InitializeComponent();
            lblOwnerName.Content = currOwner.Name;
            updateListView();
        }
        void updateListView()
        {
            try
            {
                lblId.Content = "-";
                tbMakeModel.Text = "";
                var cars = (from c in ctx.Cars where c.OwnerId == currOwner.Id select c).ToList<Car>();
                ctx.SaveChanges();
                lvCars.ItemsSource = cars;
                lvCars.Items.Refresh();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        Car selectedCar;
        private void LvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedCar = (Car)lvCars.SelectedItem;
            if (selectedCar != null)
            {
                lblId.Content = selectedCar.Id;
                tbMakeModel.Text = selectedCar.MakeModel;
            }
        }

        private void BtDone_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string makeModel = tbMakeModel.Text; // TODO: Verify value
                ctx.Cars.Add(new Car() { MakeModel = makeModel, OwnerId = currOwner.Id });
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Error saving record:", MessageBoxButton.OK, MessageBoxImage.Error); // FIXME: nicer
            }
            updateListView();
        }

        private void BtUpdate_Click(object sender, RoutedEventArgs e)
        {
            selectedCar = (Car)lvCars.SelectedItem;
            int selectedId = selectedCar.Id;
            try
            {
                Car carToUpdate = (from c in ctx.Cars where c.Id == selectedId select c).FirstOrDefault<Car>();
                if (carToUpdate != null)
                {
                    carToUpdate.MakeModel = tbMakeModel.Text;// TODO: Verify value                    
                    ctx.SaveChanges();
                    MessageBox.Show("Record updated.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                    updateListView();
                   
                }
                else
                {
                    MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this, ex.Message, "Invalid input:", MessageBoxButton.OK, MessageBoxImage.Error);
                return; // keep dialog open
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            selectedCar = (Car)lvCars.SelectedItem;
            int selectedId = selectedCar.Id;
            if (selectedCar != null)
            {
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected car?", "Program message", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        try
                        {
                            Car carToDelete = (from c in ctx.Cars where c.Id == selectedId select c).FirstOrDefault<Car>();
                            if (carToDelete != null)
                            {
                                ctx.Cars.Remove(carToDelete); // schedule for deletion from database
                                ctx.SaveChanges();
                                MessageBox.Show("Record deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                updateListView();
                               
                            }
                            else
                            {
                                MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
        }
    }
}
