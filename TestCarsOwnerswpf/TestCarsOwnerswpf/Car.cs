﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCarsOwnerswpf
{
    public class Car
    {
        public int Id{get; set;}
        [Required]
        [StringLength(100)]
        public string MakeModel { get; set; }
        public int OwnerId { get; set; }
        public Owner Owner { get; set; }

    }
}
