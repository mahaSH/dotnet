﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCarsOwnerswpf
{
    class ParkContext:DbContext
    {
        virtual public DbSet<Owner> Owners { get; set; }
        virtual public DbSet<Car> Cars { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Car>()
                .HasRequired<Owner>(s => s.Owner)
                .WithMany(g => g.CarsInGarage)
                .HasForeignKey<int>(s => s.OwnerId);
        }
    }
}
