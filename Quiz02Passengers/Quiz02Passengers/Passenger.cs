﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Quiz02Passengers
{
    public class Passenger
    {



        //CONSTRUCTOR
        public Passenger(int id, string name, string passportNo, string destination, DateTime? departureDateTime)
        {
            Id = id;
            Name = name;
            PassportNo = passportNo;
            Destination = destination;
            DepartureDateTime = departureDateTime;
        }
        //SETTERS AND GETTERS;

        public int Id { set; get; }// INT PK IDENTITY(1,1)
        //
        private string _name;
        public string Name
        {
            // VC(100) NOT NULL
            set
            {
                if (value.Length < 1 || value.Length > 100)
                {
                    MessageBox.Show("Name should be 1-100 char length",
                    "Name error",
                    MessageBoxButton.OK,
                   MessageBoxImage.Error
                    );
                    return;
                }
                _name = value;
            }
            get
            {
                return _name;
            }
        }
        //
        private string _passportNo; // VC(10) NOT NULL
        public string PassportNo
        {
            set
            {
                //Match result = Regex.Match(value, @"([A-Z]{2}\d{6}");
                if (value == null)// result.Success
                {
                    MessageBox.Show("Passport Shoud be two capital letters followed by 6 digits",
                   "Passport error",
                   MessageBoxButton.OK,
                  MessageBoxImage.Error
                   );
                    return;
                }
                _passportNo = value;
            }
            get
            {
                return _passportNo;
            }
        }
        //

        private string _destination; // VC(100) NOT NULL
        public string Destination
        {
            set
            {
                if (value.Length == 0 || value.Length > 100)
                {
                    MessageBox.Show("Destinatio should be 100 maximum char length, not null",
                    "Destination error",
                    MessageBoxButton.OK,
                   MessageBoxImage.Error
                    );
                    return;
                }
                _destination = value;
            }
            get
            {
                return _destination;
            }
        }
        //
        public  DateTime? DepartureDateTime { set; get; }// DateTime type, visible as two inputs in UI
    }
}
