﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz02Passengers
{
    class Database
    {
        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\dotnet\Quiz02Passengers\PassengersDB.mdf;Integrated Security=True;Connect Timeout=30";

        SqlConnection conn;


        public Database() // ex SqlException CONSTRUCTOR
        {
            conn = new SqlConnection(connString);
            conn.Open();
        }
        //ADD
        public void AddPassenger(Passenger passenger) // ex SqlException
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Passengers (Name,PassportNo,Destination,DepartureDateTime) VALUES (@Name,@PassportNo,@Destination,@DepartureDateTime)", conn);
            cmdInsert.Parameters.AddWithValue("@Name", passenger.Name);
            Console.WriteLine(passenger.Name);
            cmdInsert.Parameters.AddWithValue("@PassportNo", passenger.PassportNo);
            Console.WriteLine(passenger.PassportNo);
            cmdInsert.Parameters.AddWithValue("@Destination", passenger.Destination);
            Console.WriteLine(passenger.Destination);
            cmdInsert.Parameters.AddWithValue("@DepartureDateTime", passenger.DepartureDateTime);
            Console.WriteLine(passenger.DepartureDateTime);
            cmdInsert.ExecuteNonQuery();
            Console.WriteLine("shoul be added");
            
        }
        //FETCH ALL
        public List<Passenger> GetAllPassengers() // ex SqlException, ArgumentException
        {
            List<Passenger> result = new List<Passenger>();
            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Passengers", conn);
            using (SqlDataReader selectReader = cmdSelectAll.ExecuteReader())
            {// SqlException
                while (selectReader.Read())
                {
                    int id = selectReader.GetInt32(selectReader.GetOrdinal("Id"));
                    string name = selectReader.GetString(selectReader.GetOrdinal("Name"));
                    string passportNo = selectReader.GetString(selectReader.GetOrdinal("PassportNo"));
                    string destination = selectReader.GetString(selectReader.GetOrdinal("Destination"));
                    DateTime departureDateTime = selectReader.GetDateTime(selectReader.GetOrdinal("departureDateTime"));
                    Passenger newPassenger = new Passenger(id, name, passportNo, destination, departureDateTime); // ex ArgumentException
                    result.Add(newPassenger);
                }
                return result;
            }
        }
        //UPDATE 
        public void UpdatePassengers(Passenger p)
        {
            Console.WriteLine("in database");
            SqlCommand cmdUpdate = new SqlCommand(
                "UPDATE Passengers SET " +
                    "name=@Name, " +
                    "passportNo=@PassportNo, " +
                    "destination=@Destination," +
                    "DepartureDateTime=@DepartureDateTime " +
                "WHERE Id=@Id",
                conn
                );
            cmdUpdate.Parameters.AddWithValue("@Name", p.Name);
            cmdUpdate.Parameters.AddWithValue("@PassportNo", p.PassportNo);
            cmdUpdate.Parameters.AddWithValue("@Destination",p.Destination);
            cmdUpdate.Parameters.AddWithValue("@DepartureDateTime",p.DepartureDateTime);
            cmdUpdate.Parameters.AddWithValue("@Id",p.Id);
            cmdUpdate.ExecuteNonQuery();
        }
        //DELETE
        public void DeletePassenger(int Id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Passengers WHERE Id=@Id", conn);
            cmdDelete.Parameters.AddWithValue("@Id", Id);
            cmdDelete.ExecuteNonQuery();
        }
    }
}
