﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz02Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Passenger> PassengerList = new List<Passenger>();
        public List<Passenger> FilteredPassengerList = new List<Passenger>();
        public MainWindow()
        {
            InitializeComponent();
            refreshListFromDb();


        }
        void refreshListFromDb()
        {
            try
            {
                lvPassengers.ItemsSource = Globals.Db.GetAllPassengers(); // ex SqlException, ArgumentException
               /* if (tbSearch.Equals(null)){
                    PassengerList= Globals.Db.GetAllPassengers();
                    foreach(Passenger p in PassengerList)
                    {
                        if (filter(p) == true)
                        {
                            FilteredPassengerList.Add(p);
                        }
                    }
                }
                lvPassengers.ItemsSource = FilteredPassengerList;*/
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message); // FIXME: better dialog
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message); // FIXME: better dialog
            }
        }
        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialoge addEditDialog = new AddEditDialoge(null);
            addEditDialog.ShowDialog();
            refreshListFromDb();
        }

        private void LvPassengers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger p=(Passenger)lvPassengers.SelectedValue;
            if (p == null)
            {
                return;
            }
            Console.WriteLine("double click");
            AddEditDialoge addEditDialog = new AddEditDialoge(p);
            addEditDialog.ShowDialog();
            refreshListFromDb();
        }
        public bool filter(Passenger p)
        {
            string filter = tbSearch.Text;
       if(p.Name.Contains(filter)==true||p.PassportNo.Contains(filter)==true|| p.Destination.Contains(filter) == true)
            {
                return true;
            }
            return false;
        }
    }
}
