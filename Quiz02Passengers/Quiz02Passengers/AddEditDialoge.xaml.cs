﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz02Passengers
{
    /// <summary>
    /// Interaction logic for AddEditDialoge.xaml
    /// </summary>
    public partial class AddEditDialoge : Window
    {
        Passenger currentPassenger;
        public AddEditDialoge(Passenger passenger)
        {
            InitializeComponent();
            //
            // comboDepTime.SetValue
            Console.WriteLine("dialogue initialized");
            currentPassenger = passenger;
            if (currentPassenger != null)
            {
                Console.WriteLine("in if statement");
                lblId.Content = currentPassenger.Id;
                tbName.Text = currentPassenger.Name;
                tbPassNo.Text = currentPassenger.PassportNo;
                tbDestination.Text = currentPassenger.Destination;
                dpDepDate.Text = currentPassenger.DepartureDateTime+"";
               // comboDepTime.Text = currentPassenger.DepartureDateTime.TimeOfDay + "";
                btSave.Content = "Save";
            }
            else
            {
                btSave.Content = "Add";
                btDelete.IsEnabled = false;
            }
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            
            string name=tbName.Text;
            string passportNo=tbPassNo.Text;
            string destination=tbDestination.Text ;
            Console.WriteLine("fetching values");
            DateTime? dt = dpDepDate.SelectedDate;
            if (currentPassenger != null)
            {//UPDATING PROCEDURE
                int id = (int)lblId.Content;
                currentPassenger.Name = name;
                currentPassenger.PassportNo = passportNo;
                currentPassenger.Destination = destination;
                currentPassenger.DepartureDateTime = dt;
                Console.WriteLine("still inside dialogue");
                Globals.Db.UpdatePassengers(currentPassenger);
            }
            else
            {//ADDING PROCEDURE
                try
                {
                    Passenger newPassenger = new Passenger(0, name, passportNo, destination, dt);
                    Globals.Db.AddPassenger(newPassenger);

                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message,
                   "invalid input",
                   MessageBoxButton.OK,
                  MessageBoxImage.Error
                   );
                    return; // keep dialog open
                }
            }
            this.DialogResult = true;
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            Globals.Db.DeletePassenger(currentPassenger.Id);
            Close();
        }
    }
}
