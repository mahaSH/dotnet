﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What is your name?");
            string name = Console.ReadLine();
            Console.Write("What is your age?");
            string ageStr = Console.ReadLine();
            int age;

            
                if(!int.TryParse(ageStr,out age))
                {
                    Console.WriteLine("input error");
                }
                else
                {
                    Console.WriteLine("Hello {0}.you are {1}y/o,nice to meet you {0}.", name,age);
                }

            
            
            //Console.WriteLine("Hello World!");
            Console.WriteLine("Press any key to finish");
            Console.ReadKey();
        }
    }
}
