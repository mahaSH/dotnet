﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CarsOwnerrsDBContext ctx;
        public MainWindow()
        {
            try
            {
                ctx = new CarsOwnerrsDBContext();
                InitializeComponent();
                updateListView();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Fatal error connection to database:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void updateListView()
        {
            try
            {
                var owners = (from o in ctx.Owners.Include("CarsInGarage") select o).ToList<Owner>();
                ctx.SaveChanges();
                lvOwners.ItemsSource = owners;
                lvOwners.Items.Refresh();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void resetAllFields()
        {
            lblId.Content = "-";
            tbName.Text = "";
            imgPicture.Source = null;
        }

        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private BitmapImage byteArrayToBitmapImage (byte[] byteImage)
        {
            if (byteImage == null) return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new System.IO.MemoryStream(byteImage);
            image.EndInit();
            return image;
        }

        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, ex.Message,"Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error); 
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] data = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                string name = tbName.Text; // TODO: Verify value
                ctx.Owners.Add(new Owner() { Name = name, Photo = data });
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message,"Error saving record:", MessageBoxButton.OK, MessageBoxImage.Error); // FIXME: nicer
            }
            updateListView();
        }

        Owner selectedOwner;
        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedOwner =(Owner) lvOwners.SelectedItem;
            if (selectedOwner != null)
            {
                lblId.Content = selectedOwner.Id;
                tbName.Text = selectedOwner.Name;
                imgPicture.Source = byteArrayToBitmapImage (selectedOwner.Photo);
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            selectedOwner = (Owner)lvOwners.SelectedItem;
            int selectedId = selectedOwner.Id;
            try
            {
                Owner ownerToUpdate = (from o in ctx.Owners where o.Id == selectedId select o).FirstOrDefault<Owner>();
                if (ownerToUpdate != null)
                {
                    ownerToUpdate.Name = tbName.Text;// TODO: Verify value
                    ownerToUpdate.Photo = bitmapImageToByteArray((BitmapImage)imgPicture.Source);                    
                    ctx.SaveChanges();
                    MessageBox.Show("Record updated." + ownerToUpdate.ToString(), "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                    updateListView();
                    resetAllFields();
                }
                else
                {
                    MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this, ex.Message, "Invalid input:", MessageBoxButton.OK, MessageBoxImage.Error);
                return; // keep dialog open
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            selectedOwner = (Owner) lvOwners.SelectedItem;
            int selectedId = selectedOwner.Id;
            if (selectedOwner != null)
            {
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected woner?", "Program message", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        try
                        {
                            Owner ownerToDelete = (from o in ctx.Owners where o.Id == selectedId select o).FirstOrDefault<Owner>();
                            if (ownerToDelete != null)
                            {
                                
                                
                                ctx.Owners.Remove(ownerToDelete); // schedule for deletion from database
                                ctx.SaveChanges();
                                MessageBox.Show("Record deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                updateListView();
                                resetAllFields();
                            }
                            else
                            {
                                MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
        }

        private void btManageOwnerCars_Click(object sender, RoutedEventArgs e)
        {
            selectedOwner = (Owner)lvOwners.SelectedItem;            
            if (selectedOwner == null) return;
            CarsDialog carDialog = new CarsDialog(selectedOwner);
            if (carDialog.ShowDialog()== true)
            {
                updateListView();
            }
        }
    }
}
