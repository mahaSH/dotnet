﻿namespace Day11CarsOwnersEF.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Day11CarsOwnersEF.CarsOwnerrsDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Day11CarsOwnersEF.CarsOwnerrsDBContext";
        }

        protected override void Seed(Day11CarsOwnersEF.CarsOwnerrsDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
