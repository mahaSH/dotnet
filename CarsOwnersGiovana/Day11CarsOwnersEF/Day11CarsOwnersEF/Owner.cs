﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    public class Owner
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        
        [NotMapped]
        public int? CarsNumber { get { return CarsInGarage?.Count; } } // compute only, don't store in DB

        [Column(TypeName = "image")]
        public byte[] Photo { get; set; } // blob
        public ICollection<Car> CarsInGarage { get; set; } // one to many

        public override string ToString()
        {
            return string.Format("{0} and {1} has {2}", Id, Name, CarsNumber);
        }
    }
}
