﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    class CarsOwnerrsDBContext : DbContext
    {
        virtual public DbSet<Owner> Owners { get; set; }
        virtual public DbSet<Car> Cars { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<Car>()
                .HasRequired<Owner>(s => s.Owner)
                .WithMany(g => g.CarsInGarage)
                .HasForeignKey<int>(s => s.OwnerId);

            //Configure Cascade Delete using Fluent API
            /*modelBuilder.Entity<Owner>()
                .HasMany<Car>(g => g.CarsInGarage)
                .WithRequired(s => s.Owner)
                .WillCascadeOnDelete();*/
        }
    }
}

