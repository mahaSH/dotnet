﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Multi
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }
    /******************************************************************/
    class Airport
    {
        /******************LOGER AND LOGER DELEGATED***************/
        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate Logger;

        /******************CONSTRUCTORS***************************/
        
        public Airport(string code, string city, double lat, double lng, int elevM)
        {
            if (Logger != null)
            {
                Logger("Object is being created from default constructor");
            }
            Code = code;
            City = city;
            Lattude = lat;
            Longtude = lng;
            ElevationMeters = elevM;
        }
        //
        public Airport(string dataLine)
        {//correct line eg-------->YUL;Montreal;45.4697842;-73.7554174;36
            if (Logger != null)
            {
                Logger("Object is being created from dataLine constructor");
            }
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("DataLine Constructor error:Invalid line");
            }
            Code = data[0];
            City = data[1];
            //
            double lat;
            if (!double.TryParse(data[2], out lat))
            {
                throw new InvalidDataException("DataLine Constructor error:latitiude type should be double");
            }
            Lattude = lat;
            //
            double lng;
            if (!double.TryParse(data[3], out lng))
            {
                throw new InvalidDataException("DataLine Constructor error:longtude type should be double");
            }
            Longtude = lng;
            //
            int elevM;
            if (!int.TryParse(data[4], out elevM))
            {
                
                throw new InvalidDataException("DataLine Constructor error:Elevation  type should be int");
            }
            ElevationMeters = elevM;
        }
        /*******************SETTERS AND GETTERS*********************/

        private string _code; // exactly 3 uppercase letters, use regexp
        public string Code
        {
            set
            {
                var regex = new Regex(@"^[A-Z]{3}$");
                Match matchResult = regex.Match(value);
                if (!matchResult.Success)
                {
                    throw new InvalidDataException("Error:Airport code should be exactly 3 uppercase letters");
                }
                _code = value;
            }
            get
            {
                return _code;

            }

        }
        /******************************/
        private string _city;
        public string City
        { // 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
            set
            {
                /*var regex = new Regex(@"^[A-Z a_z 0-9 .,- ]*$");
                Match matchResult = regex.Match(value);*/
                if (value.Length < 1 || value.Length > 50 )//|| !matchResult.Success)
                {
                    throw new InvalidDataException("Error:City name should be  1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters");
                }
                _city = value;
            }
            get
            {
                return _city;
            }
        }
        /*********************************/
        private double _latitude;
        public double Lattude
        {// -90 to 90,
            set
            {
                if (!(value > -90 && value < 90))
                {
                    throw new InvalidDataException("Error:Latitude should be -90 to 90");
                }
                _latitude = value;
            }
            get
            {
                return _latitude;
            }
        }
        /*********************************************/
        private double _longtude;
        public double Longtude
        {// -180 to 180
            set
            {
                if (!(value > -180 && value < 180))
                {
                    throw new InvalidDataException("Error:Longtude should be -90 to 90");
                }
                _longtude = value;
            }
            get
            {
                return _longtude;
            }
        }
        /************************************/
        private int _elevationMeters;
        public int ElevationMeters
        {//   -1000 to 10000
            set
            {
                if ( !(value > -1000 && value < 10000))
                {
                    
                    throw new InvalidDataException("Error:ElevationMeters shuold be -1000 to 10000");
                }
                _elevationMeters = value;
            }
            get
            {
                return _elevationMeters;
            }
        }
        /*****************************TOSTRING*************************/
        public override string ToString()
        {//E.G--->YYZ in Toronto at 43.6777215 lat / 79.6270084 lng at 68m elevation
            return string.Format("{0} in {1} at {2} lat /{3} lng at {4}m elevation", Code, City, Lattude, Longtude, ElevationMeters);
        }
        public string ToDataString()
        {//eg-------->YUL;Montreal;45.4697842;-73.7554174;36
            return string.Format("{0};{1};{2};{3};{4}", Code, City, Lattude, Longtude, ElevationMeters);
        }
    }
}
