﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Multi
{


    class Program
    {
        static List<Airport> AirportsList = new List<Airport>();//LIST DEFINITION
        const string file = @"..\..\data.txt";
        /**************getMenuChoice() FUNCTION*****************/
        static int getMenuChoice()
        {
            while (true)
            {
                Console.Write(@"Menu:
1. Add Airport
2. List all airports
3. Find nearest airport by code (preferably using LINQ)
4. Find airport's elevation standard deviation (using LINQ)
5. Change log delegates
0. Exit
Enter your choice: ");
                int choice;
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
                if (choice < 0 || choice > 5)
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
                return choice;
            }
        }
        /**************1-addAirPort() method*********************/
        static void AddAirPort()
        {
            Console.WriteLine(@"Adding airport
Enter code: ");
            string code = Console.ReadLine();
            Console.WriteLine("Enter city: " );
            string city = Console.ReadLine();
            Console.WriteLine(" Enter latitude:  ");
            double lat;
            string latStr = Console.ReadLine();
            if (!double.TryParse(latStr, out lat)) {
                //throw new InvalidDataException("");
                Console.WriteLine("invalid input");
            }
            Console.WriteLine(" Enter longitude:  ");
            double lng;
            string lngStr = Console.ReadLine();
            if (!double.TryParse(lngStr, out lng))
            {
                //throw new InvalidDataException("");
                Console.WriteLine("invalid input");
            }
            Console.WriteLine("Enter elevation:  ");
            int elevm;
            string elevmStr = Console.ReadLine();
            if (!int.TryParse(elevmStr, out elevm))
            {
                //throw new InvalidDataException("");
                Console.WriteLine("invalid input");
            }
            Airport a = new Airport(code, city, lat, lng, elevm);
            AirportsList.Add(a);
            Console.WriteLine("Airport added successfully");
        }
        /**************2-ListAllAirports() method****************/
        static void ListAllAirports()
        {
            if (AirportsList == null)
            {
                Console.WriteLine("Empty List,nothing to be printed out!!");
            }
            foreach(Airport a in AirportsList)
            {
                Console.WriteLine(a);
            }

        }
        /**************3-findNearestAirport() method********************/
       /* static void findNearestAirport()
        {

            ListAllAirports();
            Console.WriteLine("enter airport code that you want to check");
            string airport = Console.ReadLine();

            foreach(Airport a in AirportsList)
            {
                if (a.Code != airport)
                {
                    DistanceBetweenPlaces(a.Lattude,a.Longtude)
                }
            }
        }*/
        /**************5-ChangeLogDelegates() method***********/
        static string ChangeLogDelegates()
        {
            Console.WriteLine(@"Changing logging settings:
1-Logging to console
2-Logging to file
Enter your choices, comma-separated, empty for none: ");
            string choice=Console.ReadLine();
            if ( !choice.Equals("1") && !choice.Equals("2") && !choice.Equals("1,2") && !choice.Equals("2,1") && !choice.Equals(" "))
            {
                Console.WriteLine("Invalid choice");
            }
            
            return choice;
        }
        /**************ReadDataFromFile method*****************/
        static void ReadDataFromFile()
        {
            try
            {
                string[] linesArray = File.ReadAllLines(file);
                foreach (string line in linesArray)
                {
                    try
                    {
                        Airport a = new Airport(line);
                        AirportsList.Add(a);
                    }

                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("Error parsing line `{0}`:\n{1}\nskipping.", line, ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }

            Console.WriteLine("Data read from file.");
        }
        /**************WriteDataToFile() method*****************/
        static void WriteDataToFile()
        {
            try
            {
                List<String> linesList = new List<String>();
                foreach (Airport p in AirportsList)
                {
                    string newLine = p.ToDataString();
                    linesList.Add(newLine);
                }
                File.WriteAllLines(file, linesList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }
        /**************METHODS FOR LOGGING*****************/
        static void LogToConsole(string why)
        {
            Console.WriteLine("Event Object Creation: " + why);
        }

        static void LogToFile(string why)
        {
            var time = DateTime.Now;
            File.AppendAllText( @"..\..\eventsLog.txt", time + ": " +  why + "\n");
        }
        /**************Find the distance method*************/
        static double DistanceBetweenPlaces(double lon1, double lat1, double lon2, double lat2)
        {
            double R = 6371; // km

            double sLat1 = Math.Sin((lat1));
            double sLat2 = Math.Sin((lat2));
            double cLat1 = Math.Cos((lat1));
            double cLat2 = Math.Cos((lat2));
            double cLon = Math.Cos((lon1) - (lon2));

            double cosD = sLat1 * sLat2 + cLat1 * cLat2 * cLon;

            double d = Math.Acos(cosD);

            double dist = R * d;

            return dist;
        }
        /**************Main method**************************/
        static void Main(string[] args)
        {
            ReadDataFromFile();
            Airport.Logger = null;
            while (true)
            {
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddAirPort();
                        break;
                    case 2:
                        ListAllAirports();
                        break;
                    case 3:
                        //findNearestAirport();
                        break;
                    case 4:

                        break;
                    case 5:
                        string logChoice=ChangeLogDelegates();
                        switch (logChoice)
                        {
                            case "1":
                                Airport.Logger = LogToConsole;
                                break;
                            case "2":
                                Airport.Logger = LogToFile;
                                break;
                            case "1,2":
                                Airport.Logger = LogToConsole;
                                Airport.Logger += LogToFile;
                                break;
                            case "2,1":
                                Airport.Logger = LogToConsole;
                                Airport.Logger += LogToFile;
                                break;
                            case " ":
                                Airport.Logger =null;
                                break;
                            default:
                                break;


                        }
                        break;
                    case 0:
                        WriteDataToFile();
                        Console.WriteLine("Data saved. Exiting.");
                        return; // terminate the program
                    default:
                        Console.WriteLine("Internal error: invalid control flow in menu");
                        break;
                }
            }
        }
    }
}
