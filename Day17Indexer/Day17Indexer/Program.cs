﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day17Indexer
{
    
    class PrimeArray
    {
        public long this[int index]
        {
            get
            {
                if (index <= 0)
                {
                    throw new InvalidOperationException("Index value must be 1 or greater");
                }
                return getNthPrime(index);
            }
        }

        private long getNthPrime(long n)
        {
            int num = 1;
            int count = 0;
            while (true)
            {
                if (isPrime(num))
                {
                    count++;
                    if (count == n)
                    {
                        return num;
                    }
                }
                num++;
            }
        }

        private bool isPrime(long num)
        {
            for (int i = 2; i < num; i++)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            PrimeArray pa = new PrimeArray();
            for (int i = 1; i < 10; i++)
            {
                Console.WriteLine(i + " : " + pa[i]);
            }
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}

