﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    public class Car
    {
        public int Id { set; get; }
        public string MakeModel { set; get; }
        // many to one relation
       public int OwnerId { set; get; }
        public Owner Owner { set; get; }
    }
}
