﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07CarsDB
    //CONSTRAINT CHK_FuelType   
   //CHECK (FuelType in ('Gasoline','Diesel','Hybrid','Electric','Other')) 
{
    //constructor will connect to the database and 4 methods will be added implementing CRUD.  AddCar, GetAllCars, UpdateCar, DeleteCar.
    class Database
    {
        //const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\ABDULLAH\Desktop\carsDB.mdf;Integrated Security=True;Connect Timeout=30";
        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\dotnet\Day07SimpleDataBase\Day07SimpleDataBase\carDataBase.mdf;Integrated Security=True;Connect Timeout=30";
        //
        static SqlConnection conn = new SqlConnection(connString);
       
       //ADD CAR
       public static  void AddToDB(string makeModel, double engSizeL, Car.FuelTypeEnum fuelType)
        {
            conn.Open();
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Cars (makeModel,EngineSizeL,fuelType) VALUES (@makeModel,@EngineSizeL,@fuelType)", conn);
            cmdInsert.Parameters.AddWithValue("@makeModel", makeModel);
            cmdInsert.Parameters.AddWithValue("@EngineSizeL", engSizeL);
            cmdInsert.Parameters.AddWithValue("@fuelType", fuelType);
            cmdInsert.ExecuteNonQuery();
        }
       //GET ALL CARS
       public static void GetAllFromDB()
        {
            conn.Open();
            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Cars", conn);
            SqlDataReader selectReader = cmdSelectAll.ExecuteReader();
            while (selectReader.Read())
            {
                int id = selectReader.GetInt32(0);
                string makeModel = selectReader.GetString(1);
                Car.FuelTypeEnum fuelType = (Car.FuelTypeEnum)Enum.Parse(typeof(Car.FuelTypeEnum), selectReader.GetString(3));
                double engineSize= selectReader.GetDouble(2);
                Car car = new Car(id, makeModel, engineSize, fuelType);
                MainWindow.carsList.Add(car);

            }

        }
        public void UpdateCar(Car car)
        {

        }
    }
}
