﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07CarsDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static  List<Car> carsList = new List<Car>();

        public MainWindow()
        {
            InitializeComponent();
            Database.GetAllFromDB();
            lvCars.ItemsSource = carsList;
            refreshListAndStatus();
        }

        private void refreshListAndStatus()
        {
            lvCars.Items.Refresh(); // list was modified - refresh display
            lblStatus.Text = String.Format("There are {0} item(s) on the list", carsList.Count);
        }

        private void miFileExportToCSV_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                using (var writer = new StreamWriter(saveFileDialog.FileName))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(carsList);
                }
            }

        }

        private void miAddCar_MenuClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDialog = new AddEditDialog(carsList, null);
            addEditDialog.ShowDialog();
            refreshListAndStatus();
        }

        private void miFileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void miContextEdit_MenuClick(object sender, RoutedEventArgs e)
        {
            Car car = (Car)lvCars.SelectedValue;
            if (car == null)
            {
                return;
            }
            AddEditDialog addEditDialog = new AddEditDialog(carsList, car);
           refreshListAndStatus();
        }

        private void miContextDelete_MenuClick(object sender, RoutedEventArgs e)
        {
            Car car = (Car)lvCars.SelectedValue;
            if (car == null)
            {
                return;
            }
            // TODO: show confirmation dialog
            // if user confirmed remove item from carsList and refresh lvCars
        }

        private void lvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            miContextEdit_MenuClick(sender, e); // same as context menu -> edit clicked
        }
    }
}
