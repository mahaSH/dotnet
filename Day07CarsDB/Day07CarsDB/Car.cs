﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07CarsDB
{
   public class Car
    {
        public Car(int id,string makeModel, double engSizeL, FuelTypeEnum fuelType)
        {
            Id = id;
            MakeModel = makeModel;
            EngineSizeL = engSizeL;
            FuelType = fuelType;
        }
        public Car(string makeModel, double engSizeL, FuelTypeEnum fuelType)
        {
            MakeModel = makeModel;
            EngineSizeL = engSizeL;
            FuelType = fuelType;
        }
        public int Id { get; set; } // 2-50 characters, no semicolons
        public string MakeModel { get; set; } // 2-50 characters, no semicolons
        
        public double EngineSizeL { get; set; } // 0-20
        
        public FuelTypeEnum FuelType { get; set; }
        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other }
    }
}

