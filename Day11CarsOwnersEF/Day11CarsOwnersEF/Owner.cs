﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    public class Owner
    {
        public int Id{set; get; }
        [Required]
        [StringLength(100)]
        public string Name { set; get; }
        [NotMapped]
        public int CarsNumber { get { return CarsInGarage.Count(); } } // compute only, don't store in DB
        public byte[] Photo { set; get; } // blob
        public ICollection<Car> CarsInGarage { set; get; } // one to many
    }
   
}
