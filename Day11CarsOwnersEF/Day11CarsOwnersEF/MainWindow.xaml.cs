﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       static  CarsOwnersDbContext2 ctx;
        Owner SelectedOwner;
        //INITIALIZE
        public MainWindow()
        {
            try
            {
                ctx = new CarsOwnersDbContext2();
                InitializeComponent();
                refreshList();
            }catch(SystemException ex)
            {
                MessageBox.Show("Fatal error connecting to database:\n" + ex.Message); 
            }
        }
        //MANAGE CARS
        private void BtManage_Click(object sender, RoutedEventArgs e)
        {
            CarDialog dialoge = new CarDialog(SelectedOwner);
            dialoge.ShowDialog();
            refreshList();


        }

    //REFRESH
        public void refreshList()
        {
           
            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;
            btManage.IsEnabled = false;
            lblId.Content = "...";
            tbName.Text="";
            List<Owner> OwnersList = new List<Owner>();
            var items = ctx.Owners.Include(Owner.CarsInGarage).ToList();
           // var items = (from p in ctx.Owners select p).ToList();
            foreach (var item in items)
            {
                OwnersList.Add(item);
            }
            lvOwners.ItemsSource = OwnersList;
        }
        //ADD
        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] data = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                string name = tbName.Text; // TODO: Verify value
                ctx.Owners.Add(new Owner() { Name = name, Photo = data });
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error saving record:\n" + ex.Message); // FIXME: nicer
            }
        }
        //BITMAPIMAGE TO ARRAY OF BYTES
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }
        //BLOB CLICKED
        private void BtBlob_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error opening file:\n" + ex.Message); // FIXME: nicer
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show("Error opening file:\n" + ex.Message); // FIXME: nicer
                }
            }

        }

        //ARRAY OF BYTES TO IMAGE
        public BitmapImage ToImage(byte[] array)
        {
           // if (array.Length == 0) return null;
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }
        //LIST DOUBLE CLICK
        private void LvOwners_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Owner owner = (Owner)lvOwners.SelectedValue;
            if (owner == null)
            {
                return;

            }
            lblId.Content = owner.Id+"";
            tbName.Text = owner.Name;
            imgPicture.Source = ToImage(owner.Photo);
            btDelete.IsEnabled = true;
            btUpdate.IsEnabled = true;
            btManage.IsEnabled = true;
            //
            SelectedOwner = owner;
        }
        //UPDATE
        private void BtUpdate_Click(object sender, RoutedEventArgs e)
        { 
            try
            {
                ctx = new CarsOwnersDbContext2();
                Owner editOwner = (from p in ctx.Owners where p.Id == SelectedOwner.Id select p).FirstOrDefault<Owner>();
                if (editOwner != null)
                {
                    editOwner.Name = tbName.Text;
                    byte[] data = 
                    editOwner.Photo = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                    ctx.SaveChanges();
                    refreshList();
                }
   
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error opening file:\n" + ex.Message); // FIXME: nicer
            }

        }
        //DELETE
        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            ctx = new CarsOwnersDbContext2();
            Owner deleteOwner= (from p in ctx.Owners where p.Id == SelectedOwner.Id select p).FirstOrDefault<Owner>();
            if (deleteOwner != null)
            {
                ctx.Owners.Remove(deleteOwner);
                ctx.SaveChanges();
                refreshList();
            }
            else
            {
                return;
            }
        }
    }
    }

