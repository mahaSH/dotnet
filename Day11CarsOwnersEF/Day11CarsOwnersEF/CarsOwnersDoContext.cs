﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    class CarsOwnersDbContext2 : DbContext
    {
        virtual public DbSet<Owner>Owners{ get; set; }
        virtual public DbSet<Car> Cars { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // configures one-to-many relationship
            modelBuilder.Entity<Car>()
                .HasRequired<Owner>(c => c.Owner )
                .WithMany(o => o.CarsInGarage)
                .HasForeignKey<int>(o => o.OwnerId);
            
        }
    }
}