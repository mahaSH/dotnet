﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for ManageCarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {
        CarsOwnersDbContext2 ctx;
        Owner carOwner;
        public CarDialog(Owner owner)
        {
            //ctx = new CarsOwnersDbContext2();
            carOwner = owner;
            InitializeComponent();
            lblOwnerName.Content = owner.Name;
            refreshCarList();
            /*using (var context = new SchoolDBEntities())
{
    var stud1 = (from s in context.Students.Include("Standard")
                where s.StudentName == "Bill"
                select s).FirstOrDefault<Student>();
}*/
            ctx = new CarsOwnersDbContext2();

            var cars = (from oc in ctx.Cars
                        where oc.OwnerId == carOwner.Id
                        select oc).FirstOrDefault<Car>();



        }
        public void refreshCarList()
        {
            try
            {
                btDeleteCar.IsEnabled = false;
                btUpdateCar.IsEnabled = false;
                lblCarId.Content = carOwner.Id;
                List<Car> CarsList = new List<Car>();
                //var items = ctx.Cars.ToList();
                var items = (from c in ctx.Cars where c.OwnerId == carOwner.Id select c).ToList();
                foreach (var item in items)
                {
                    CarsList.Add(item);
                }
                lvCars.ItemsSource = CarsList;
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error saving record:\n" + ex.Message); // FIXME: nicer
            }
        } 
        
    
        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            try
             {
            string makeModel = tbMakeModel.Text;
            ctx.Cars.Add(new Car() { MakeModel=makeModel, Owner= carOwner });
            ctx.SaveChanges();
        }
             catch (SystemException ex)
             {
                 MessageBox.Show("Error saving record:\n" + ex.Message); // FIXME: nicer
             }
            refreshCarList();
        }

        private void BtDone_Click(object sender, RoutedEventArgs e)
        {

            
            Close();

        }
    }
}
