﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Day01SimpleFileIO
{
    class Program
    {
        const string NameFileName = @"..\..\names.txt";
        static void Main(string[] args)
        {
            //hard way
            using (StreamWriter sw = File.AppendText(@"..\..\output.txt"))//exception?
            {
                sw.WriteLine("Line Appended");
                //Exception?

            }//sw is closed here
            //easy way
            string[] linesArray = { "jerry", "terry", "merry" };
            File.AppendAllLines(@"..\..\names.txt", linesArray);

            string[] dataBack = File.ReadAllLines(@"..\..\names.txt");

            string allText=File.ReadAllText(@"..\..\names.txt");
            Console.WriteLine("press any key to continue");

            Console.ReadKey();
        }
    }
}
