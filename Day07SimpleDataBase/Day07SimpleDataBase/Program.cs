﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07SimpleDataBase
{
    class Program
    {
        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\dotnet\Day07SimpleDataBase\SimpleDB.mdf;Integrated Security=True;Connect Timeout=30";
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();
            //
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO People(Name,Age) VALUES(@Name,@Age)", conn);
            cmdInsert.Parameters.AddWithValue("@Name","Jerry");
            cmdInsert.Parameters.AddWithValue("@Age", 33);
            cmdInsert.ExecuteNonQuery();
            //
           SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM People", conn);
            SqlDataReader selectReader = cmdSelectAll.ExecuteReader();
            while (selectReader.Read())
            {
                int id = selectReader.GetInt32(0);
                string name = selectReader.GetString(1);
                int age = selectReader.GetInt32(2);
                Console.WriteLine("{0}:{1} iS{2} years old", id, name, age);

            }
        }

        
    }
}
