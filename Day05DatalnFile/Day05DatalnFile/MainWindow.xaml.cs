﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05DatalnFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        string currentFile;
        Boolean isModified;
        private void miFileNew_MenuClick(object sender, RoutedEventArgs e)
        {
            if (isModified)
            {
                MessageBoxResult result = MessageBox.Show(this, "Text changed, do you want to save?", "New Text", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        miFileSave_MenuClick(sender, e);
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
            txtEditor.Text = "";
            currentFile = null;
            isModified = false;
        }
        private void miFileOpen_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    string fileName = openFileDialog.FileName;
                    txtEditor.Text = File.ReadAllText(fileName);
                    currentFile = fileName;
                    isModified = false;
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "Error reading file:\n" + ex.Message, "File access error", MessageBoxButton.OK);
                }
            }
        }
        private void miFileSave_MenuClick(object sender, RoutedEventArgs e)
        {
            if (currentFile == null)
            {
                miFileSaveAs_MenuClick(sender, e);
                return;
            }

            try
            {
                File.WriteAllText(currentFile, txtEditor.Text);
                isModified = false;
            }
            catch (IOException ex)
            {
                MessageBox.Show(this, "Error writing file:\n" + ex.Message, "File access error", MessageBoxButton.OK);
            }

        }
 private void miFileSaveAs_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    string fileName = saveFileDialog.FileName;
                    File.WriteAllText(fileName, txtEditor.Text);
                    currentFile = fileName;
                    isModified = false;
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "Error reading file:\n" + ex.Message, "File access error", MessageBoxButton.OK);
                }
            }
        }
        private void TxtEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            isModified = true;
        }
        private void miFileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            if (isModified)
            {
                MessageBoxResult result = MessageBox.Show(this, "Text changed, do you want to save?", "Exit Program", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        miFileSave_MenuClick(sender, e);
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
            Close();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isModified)
            {
                MessageBoxResult result = MessageBox.Show(this, "Text changed, do you want to save?", "Exit Program", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        miFileSave_MenuClick(sender, null);
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        return;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
            e.Cancel = false;
        }
    }
    
}

