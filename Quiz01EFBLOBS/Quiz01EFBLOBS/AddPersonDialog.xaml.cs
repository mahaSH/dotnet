﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz01EFBLOBS
{
    /// <summary>
    /// Interaction logic for AddPersonDialog.xaml
    /// </summary>
    public partial class AddPersonDialog : Window
    {
        PeoplexPassportDbContext ctx;

        public AddPersonDialog()
        {
            InitializeComponent();
            ctx = new PeoplexPassportDbContext();

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                int age = int.Parse(tbAge.Text);
                ctx.People.Add(new Person() { Name = name, Age = age });
                ctx.SaveChanges();
                this.DialogResult = true;
                Close();
            }
            catch(SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Error saving record:", MessageBoxButton.OK, MessageBoxImage.Error); // FIXME: nicer
            }
            
        }
    }
}
