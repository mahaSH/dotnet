﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz01EFBLOBS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PeoplexPassportDbContext ctx;
        Person selectedPerson;
        public MainWindow()
        {
            InitializeComponent();
            ctx = new PeoplexPassportDbContext();
            refreshList();

        }
        //REFRESHLIST FUNCTION
        void refreshList()
        {
            try
            {
                var people = (from o in ctx.People select o).ToList<Person>();
                ctx.SaveChanges();
                lvPerson.ItemsSource = people;
                lvPerson.Items.Refresh();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddPersonDialog personDialoge = new AddPersonDialog();
            personDialoge.ShowDialog();
            refreshList();

        }

        private void LvPerson_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            selectedPerson = (Person)lvPerson.SelectedItem;
            if (selectedPerson == null)
            {
                return;
            }
            PassportDialog passportDialog = new PassportDialog(selectedPerson);
            passportDialog.ShowDialog();
            refreshList();
        }
    }
}
