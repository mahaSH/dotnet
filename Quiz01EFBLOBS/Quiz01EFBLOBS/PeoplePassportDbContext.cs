﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Quiz01EFBLOBS
{
    class PeoplexPassportDbContext : DbContext
    {
        virtual public DbSet<Person> People { get; set; }
        virtual public DbSet<Passport> Passport { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            // Configure PersonID as FK for User
            modelBuilder.Entity<Person>()
                 .HasOptional(s => s.Passport) // Mark Pasport property optional in Student entity
                 .WithRequired (pp => pp.Person); //mark Person property as required in Passport entity

        }
    }
}
