﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz01EFBLOBS
{
    /// <summary>
    /// Interaction logic for PassportDialog.xaml
    /// </summary>
    public partial class PassportDialog : Window
    {
        PeoplexPassportDbContext ctx;
        Person selected;
        public PassportDialog(Person selectedPerson)
        {
            selected = selectedPerson;
            InitializeComponent();
            lblName.Content = selectedPerson.Name;
            lblId.Content= selectedPerson.Id;
            ctx = new PeoplexPassportDbContext();
            /*if (selected.PassportNo == null)
            {
                btSave.Content ="Add";
            }
            else*/
                btSave.Content = "Add";
        }
        //
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private BitmapImage byteArrayToBitmapImage(byte[] byteImage)
        {
            if (byteImage == null) return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new System.IO.MemoryStream(byteImage);
            image.EndInit();
            return image;
        }
        private void BtPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();

        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            /*try
            {*/
                string passportNo = tbPassport.Text;
                byte[] image = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                ctx.Passport.Add(new Passport() { PassportNo = passportNo, Photo = image,PersonId=selected.Id });
                ctx.SaveChanges();
                this.DialogResult = true;
                Close();
            /*}
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Error saving record:", MessageBoxButton.OK, MessageBoxImage.Error); // FIXME: nicer
            }*/
            
        }
    }
}
