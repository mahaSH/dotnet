﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Quiz01EFBLOBS
{
    public class Person
    {
        public int Id { get; set; }
        [Required]
        public string _name;
        public String Name
        {
            set
            {
                if (value.Length == null)
                {
                    MessageBox.Show("Name could not be null", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                _name = value;
            }
            get
            {
                return _name;
            }
        }
        [Required]
        public int _age;
        public int Age
        {
            set
            {
                if (value < 0)
                {
                    MessageBox.Show("No age value entered", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                _age = value;
            }
            get
            {
                return _age;
            }
        }
       
       public int? PassportId { get { return Passport.Id; } }
        public virtual Passport Passport { get; set; }
    }
}
