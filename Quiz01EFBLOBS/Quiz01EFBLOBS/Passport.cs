﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Quiz01EFBLOBS
{
    public class Passport
    {
        public int Id { get; set; }
        public string _passportNo;
        public string PassportNo//
        {
            set {
                var regex = @"^[A-Z]{2}\d{6}";
                var match = Regex.Match(value, regex);

                if (!match.Success)
                {
                    // does not match
                    MessageBox.Show("Passport should be 2 capital letters followed by 6 digits", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                _passportNo = value;
            }
            get
            {
                return _passportNo;
            }
        }
       [Column(TypeName = "image")]
        public byte[] Photo { get; set; } // blob
        //[ForeignKey("Person")]
        public int PersonId { get; set; }
       public virtual Person Person{ get;set;}
        
    }
}
