﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CarsOwnersContext1 ctx;
        public MainWindow()
        {
            InitializeComponent();

            ctx = new CarsOwnersContext1(); // TODO: exception

            lvOwners.ItemsSource = (from owner in ctx.Owners.Include("Cars") select owner).ToList<Owner>(); //TODO: exception 

            btUpdate.IsEnabled = false;
            btDelete.IsEnabled = false;
            btManageCars.IsEnabled = false;
            ImgPhoto.Width = 129;
            ImgPhoto.Height = 100;
        }


        public byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }

        public static ImageSource ByteToImageSource(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();

            ImageSource imgSrc = biImg as ImageSource;

            return imgSrc;
        }

        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Owner selOwner = (Owner)lvOwners.SelectedItem;
            if (selOwner == null)
            {
                txtId.Text = "-";
                tbName.Text = "";
                ImgPhoto.Source = null;
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
                btManageCars.IsEnabled = false;
                return;
            }

            txtId.Text = selOwner.Id.ToString();
            tbName.Text = selOwner.Name;
            ImgPhoto.Source = ByteToImageSource(selOwner.Photo);
            btUpdate.IsEnabled = true;
            btDelete.IsEnabled = true;
            btManageCars.IsEnabled = true;
        }

        private void btPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openPic = new OpenFileDialog();
            openPic.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
            if (openPic.ShowDialog() == true)
            {
                Uri imgUri = new Uri(openPic.FileName);
                
                ImgPhoto.Source = new BitmapImage(imgUri);
            }
            else
            {
                ImgPhoto.Source = null;
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] photo = ImageSourceToBytes(new PngBitmapEncoder(), ImgPhoto.Source);

                ctx.Owners.Add(new Owner(
                    tbName.Text,
                    photo
                    ));
                ctx.SaveChanges();
                lvOwners.ItemsSource = (from owner in ctx.Owners select owner).ToList<Owner>(); //TODO: exception 
                lvOwners.SelectedItem = null;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }


        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            Owner selOwner = (Owner)lvOwners.SelectedItem;
            if(selOwner == null)
            {
                MessageBox.Show(this,
                    "Choose one of owner in the list",
                    "No Item Selected",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            try
            {
                byte[] photo = ImageSourceToBytes(new PngBitmapEncoder(), ImgPhoto.Source); //TODO: exception
                selOwner.Name = tbName.Text;
                selOwner.Photo = photo;
                ctx.SaveChanges();
                lvOwners.ItemsSource = (from owner in ctx.Owners select owner).ToList<Owner>(); //TODO: exception 
                lvOwners.SelectedItem = null;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            Owner selOwner = (Owner)lvOwners.SelectedItem;
            if (selOwner == null)
            {
                MessageBox.Show(this,
                    "Choose one of owner in the list",
                    "No Item Selected",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            ctx.Owners.Remove(selOwner);
            ctx.SaveChanges();

            lvOwners.ItemsSource = (from owner in ctx.Owners select owner).ToList<Owner>();

            // TODO: cascade Cars on delete
        }

        private void btManageCars_Click(object sender, RoutedEventArgs e)
        {
            Owner selOwner = (Owner)lvOwners.SelectedItem;
            if (selOwner == null)
            {
                MessageBox.Show(this,
                    "Choose one of owner in the list",
                    "No Item Selected",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            CarsDialog dlg = new CarsDialog(selOwner,ctx);
            if(dlg.ShowDialog() == true)
            {
                lvOwners.ItemsSource = (from owner in ctx.Owners select owner).ToList<Owner>();
            }
        }
    }
}
