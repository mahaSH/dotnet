﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    public class CarsOwnersContext1 : DbContext
    {
        public CarsOwnersContext1()
        {
            //this.Configuration.LazyLoadingEnabled = false;   // For LazyLoading
            //this.Configuration.ProxyCreationEnabled = true; // For LazyLoading
        }
        virtual public DbSet<Owner> Owners { get; set; }
        virtual public DbSet<Car> Cars { get; set; }
    }
}
