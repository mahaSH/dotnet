﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            SocietyDBContext ctx = new SocietyDBContext();
            //Equivilant to insert
            Person p1 = new Person { Name = "Jerry", Age = random.Next(),Gender=Person.GenderEnum.Male };//state:new, detached
            ctx.People.Add(p1);//Insert operation is scheduled here but Not executed yet
            ctx.SaveChanges();
            Console.WriteLine("Record Daaed");
            //equivilant to update-fetch then modify then change
           Person p2 = (from p in ctx.People where p.Id == 1 select p).FirstOrDefault<Person>();
            if(p2!= null)
            {
                p2.Name = "Alibab";
                ctx.SaveChanges();
                Console.WriteLine("Record updated");

            }
            else
            {
                Console.WriteLine("No such Record found");
            }
            //equivilant to Delete-fetch then schedule for deletion from database
            Person p3 = (from p in ctx.People where p.Id == 3 select p).FirstOrDefault<Person>();
            if (p3 != null)
            {
                ctx.People.Remove(p3);
                ctx.SaveChanges();
                Console.WriteLine("Record Deleted");

            }
            else
            {
                Console.WriteLine("No such Record found");
            }
            //
            Console.WriteLine("press any key");
            Console.ReadKey();

        }
    }
}
