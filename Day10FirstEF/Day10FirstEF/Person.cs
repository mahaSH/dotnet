﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    //[Table("Personalitiess")]
       class Person
    {
      public enum GenderEnum { Male = 1, Female = 2, NA = 3 }
        //[Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        
        public string Name { get; set; }
        [Index]
        
        public int Age { get; set; }
        /*
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string email { get; set; }*/
        [EnumDataType(typeof(GenderEnum))]
        public GenderEnum Gender { get; set; }
        [NotMapped]//In memory only ,not on the database
        public string comment { get; set; }
}
}
