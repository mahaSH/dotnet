﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09ToDoDB
{
   

        static class Globals
        {
            private static Database _db; // TODO: maybe use singleton instead
            public static Database Db // ex SqlException
            {
                get
                {
                    if (_db == null)
                    {
                        _db = new Database();
                    }
                    return _db;
                }
            }
        }
    }
