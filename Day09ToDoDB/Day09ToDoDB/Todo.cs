﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09ToDoDB
{
   
    public class Todo
    {
        public Todo(int id, string task, DateTime? dueDate, TaskStatusEnum status)
        {
            Id = id;
            Task = task;
            DueDate = dueDate;
            Status = status;
        }
        public int Id { set; get; }
       public string Task { set; get; }
      public   DateTime? DueDate { set; get; }
        public TaskStatusEnum Status { set; get; }
        public enum TaskStatusEnum { Done, Pending };
    }
}
