﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09ToDoDB
{
    class Database
    {
        //  CONSTRAINT [CHK_Status] CHECK ([status]='Pending' OR [status]='Done')

        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\dotnet\Day09ToDoDB\TodoDB.mdf;Integrated Security=True;Connect Timeout=30";

        SqlConnection conn;


        public Database() // ex SqlException CONSTRUCTOR
        {
            conn = new SqlConnection(connString);
            conn.Open();
        }


        public void AddTask(Todo task) // ex SqlException
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Tasks (task,dueDate,status) VALUES (@task,@dueDate,@status)", conn);
            cmdInsert.Parameters.AddWithValue("@Task", task.Task);
            cmdInsert.Parameters.AddWithValue("@DueDate", task.DueDate);
            cmdInsert.Parameters.AddWithValue("@Status",task.Status);
            cmdInsert.ExecuteNonQuery();
            Console.WriteLine("shoul be added");
            /*int id = (int)cmdInsert.ExecuteScalar();
            return id;*/
        }
        //DELETE RECORD
        public void DeleteTask(int Id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Tasks WHERE Id=@Id", conn);
            cmdDelete.Parameters.AddWithValue("@Id", Id);
            cmdDelete.ExecuteNonQuery();
        }

        //FETCHALL RECORDS
        public List<Todo> GetAllTasks() // ex SqlException, ArgumentException
        {
            List<Todo> result = new List <Todo>();
            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Tasks", conn);
            SqlDataReader selectReader = cmdSelectAll.ExecuteReader(); // SqlException
            while (selectReader.Read())
            {
                int id = selectReader.GetInt32(selectReader.GetOrdinal("Id"));
                Console.WriteLine(id);
                string task = selectReader.GetString(selectReader.GetOrdinal("Task"));
                Console.WriteLine(task);
                DateTime dueDate = selectReader.GetDateTime(selectReader.GetOrdinal("DueDate"));
                Console.WriteLine(dueDate+"");
                Todo.TaskStatusEnum status = (Todo.TaskStatusEnum) Enum.Parse(typeof(Todo.TaskStatusEnum), selectReader.GetString(selectReader.GetOrdinal("status")));
                Todo newTask = new Todo(id, task, dueDate, status); // ex ArgumentException
                result.Add(newTask);
            }
            return result;
        }
        //UPDATE
        public void UpdateTask(Todo task)
        {
            SqlCommand cmdUpdate = new SqlCommand(//@Task, @DueDate, @Status
                "UPDATE Tasks SET " +
                    "task=@Task, " +
                    "DueDate=@DueDate, " +
                    "status= @Status " +
                "WHERE Id=@Id",
                conn
                );
            cmdUpdate.Parameters.AddWithValue("@Task", task.Task);
            cmdUpdate.Parameters.AddWithValue(" @DueDate", task.DueDate);
            cmdUpdate.Parameters.AddWithValue("@Status", task.Status);
            cmdUpdate.Parameters.AddWithValue("@Id", task.Id);
            cmdUpdate.ExecuteNonQuery();
        }
        //DELETE


    }
}
