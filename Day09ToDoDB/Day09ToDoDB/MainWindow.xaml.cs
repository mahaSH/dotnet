﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09ToDoDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Todo> TodoList = new List<Todo>();
        public MainWindow()
        {
            InitializeComponent();
            refreshListFromDb();
        }
        // 
        void refreshListFromDb()
        {
            try
            {
               // lvTasks.ItemsSource = Globals.Db.GetAllTasks(); // ex SqlException, ArgumentException
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message); // FIXME: better dialog
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message); // FIXME: better dialog
            }
        }

        private void LvTasks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void miFileExportToCSV_MenuClick(object sender, RoutedEventArgs e)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                TodoList= Globals.Db.GetAllTasks();
                using (var writer = new StreamWriter(saveFileDialog.FileName))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(TodoList);
                }
            }

        }

        private void miFileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();

        }

        private void miContextDelete_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditTodo addEditDialog = new AddEditTodo( null);
            addEditDialog.ShowDialog();
            refreshListFromDb();
        }

        private void RbName_Checked(object sender, RoutedEventArgs e)
        {
            //TodoList = Globals.Db.GetAllTasks();
            var nameSorted = from t in TodoList orderby t.Task select t;
            //int i= 0;
            foreach(Todo t in nameSorted){
                
                Console.WriteLine(t);
            }
            lvTasks.Items.Add(nameSorted);
        }
    }
}
