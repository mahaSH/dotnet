﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09ToDoDB
{
    /// <summary>
    /// Interaction logic for AddEditTodo.xaml
    /// </summary>
    public partial class AddEditTodo : Window
    {
        List<Todo> tasks = new List<Todo>();
        Todo currentTask;
        public AddEditTodo(Todo task)
        {
            
            InitializeComponent();

            //this.mainCarList = mainCarList;
            currentTask = task;
            if (currentTask != null)//Update case
            {
                btSave.Content = "Update";
                // TODO: load car into UI
                lblId.Content = task.Id;
                tbTask.Text = task.Task;
                dpDueDate.Text =task.DueDate+"";
                if (task.Status == Todo.TaskStatusEnum.Done)
                {
                    cbStatus.IsChecked = true;
                }
                else
                {
                    cbStatus.IsChecked = false;
                }
            }
            else
            {
                btSave.Content = "Create";
            }

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
       {

        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {   
            string task = tbTask.Text;
            DateTime? dueDate =dpDueDate.SelectedDate;
            Todo.TaskStatusEnum status;
            if (cbStatus.IsChecked==true)
            {
                status = Todo.TaskStatusEnum.Done;

            }
            else
            {
                status = Todo.TaskStatusEnum.Pending;
            }
            //
            if (currentTask != null)
            {//call update database
                try
                {
                    currentTask.Task = task;
                    currentTask.DueDate = dueDate.Value;
                    currentTask.Status = status;
                    Globals.Db.UpdateTask(currentTask);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Invalid input:\n" + ex.Message); // FIXME: better dialog
                    return; // keep dialog open
                }

            }
            else
            {

                try
                {
                    Todo newTask = new Todo(0,task, dueDate, status);
                    Globals.Db.AddTask(newTask);

                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Invalid input:\n" + ex.Message); // FIXME: better dialog
                    return; // keep dialog open
                }
            }
            this.DialogResult = true;
        }
    }
}
