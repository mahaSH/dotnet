﻿CREATE TABLE [dbo].[Tasks ] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [task]    NVARCHAR (50) NOT NULL,
    [DueDate] DATETIME NOT NULL,
    [status]  NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
  
);

