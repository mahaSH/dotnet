﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08Calc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum StateEnum { Entering1stVal, Entering2ndVal, DisplayingResult }
        StateEnum calculaterState;
        public MainWindow()
        {
            InitializeComponent();
            
        }
        private void resetCalc()
        {
            calculaterState = StateEnum.Entering1stVal;
            tbFirstNumber.Text = "";
            tbFirstNumber.IsEnabled = true;
            tbLastNumber.Text = "";
            tbLastNumber.IsEnabled = false;
            tbResult.Text = "";
            tbResult.IsEnabled = false;
            tbOperation.Text= "";
        }
        private void setNumbersButtons(bool isEnabled)
        {
            bt0.IsEnabled = isEnabled;
            bt1.IsEnabled = isEnabled;
            bt2.IsEnabled = isEnabled;
            bt3.IsEnabled = isEnabled;
            bt4.IsEnabled = isEnabled;
            bt5.IsEnabled = isEnabled;
            bt6.IsEnabled = isEnabled;
            bt7.IsEnabled = isEnabled;
            bt8.IsEnabled = isEnabled;
            bt9.IsEnabled = isEnabled;
            btDot.IsEnabled = isEnabled;
            btSign.IsEnabled = isEnabled;
        }
        private void setOperationsButtons(bool isEnabled)

        {
            btSum.IsEnabled = isEnabled;
            btMinus.IsEnabled = isEnabled;
            btMulti.IsEnabled = isEnabled;
            btDevide.IsEnabled = isEnabled;
        }

            private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           

            
        }

        private void btNumber_Click(object sender, RoutedEventArgs e)
        {
            TextBox tbCurrent;
            if (calculaterState == StateEnum.Entering1stVal)
            {
                tbCurrent = tbFirstNumber;
            }
            else if (calculaterState == StateEnum.Entering2ndVal)
            {
                tbCurrent = tbLastNumber;
            }
            else
            {
                return; // result is being displayed, number buttons should not work
            }
            //
            Button button = (Button)sender;
            string text = button.Content.ToString();
            switch (text)
            {
                case "+/-":
                    if (tbCurrent.Text == "")
                    {
                        return;
                    }
                    if (tbCurrent.Text[0] == '-') // is first character '-'
                    {
                        string dispText = tbCurrent.Text;
                        tbCurrent.Text = dispText.Substring(1, dispText.Length - 1);
                    }
                    else
                    {
                        tbCurrent.Text = "-" + tbCurrent.Text;
                    }
                    break;
                case ".":
                    tbCurrent.Text += text;
                    break;
                default:
                    tbCurrent.Text += text;
                    break;

            }
           
        }
        private void btOperation_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            string text = button.Content.ToString();
            tbOperation.Text = text;
            calculaterState = StateEnum.Entering2ndVal;
            tbFirstNumber.IsEnabled = false;
            tbLastNumber.IsEnabled = true;
            
        }
        private void btEqual_Click(object sender, RoutedEventArgs e)
        {
            calculaterState = StateEnum.DisplayingResult;
            string text = tbOperation.Text;
            tbFirstNumber.IsEnabled = false;
            tbLastNumber.IsEnabled = false;
            switch (text)
            {
                case "+":
                    tbResult.Text=(double.Parse( tbLastNumber.Text) + double.Parse(tbFirstNumber.Text))+"";
                    break;
                case "-":
                    tbResult.Text = (double.Parse(tbFirstNumber.Text) -double.Parse(tbLastNumber.Text)) + "";
                    break;
                case "*":
                    tbResult.Text = (double.Parse(tbFirstNumber.Text) *double.Parse(tbLastNumber.Text)) + "";
                    break;
                case "/":
                    tbResult.Text = (double.Parse(tbFirstNumber.Text)/double.Parse(tbLastNumber.Text)) + "";
                    break;
                default:
                    break;

            }
        }
        private void btCE_Click(object sender, RoutedEventArgs e)
        {
            resetCalc();
        }

        private void textBoxFloatingNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            /*TextBox textBox = (TextBox)sender;
            double val;
            e.Handled = double.TryParse(textBox.Text, out val);*/
        }
    }
}
