﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void TxInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            string input = tbInput.Text;
            //C TO C
            if (rbInputScaleC.IsChecked == true  && rbOutputScaleC.IsChecked == true)
            {
                string output = string.Format("{0} \u00B0C", input);
                lblOutput.Content=output;//setText;
            }
            else if (rbInputScaleC.IsChecked == true && rbOutputScaleF.IsChecked == true)
            {
                double converted = Convert.ToDouble(input) * 9 / 4 + 32;
                string inputStr = string.Format("{0}", converted);
                string output = string.Format("{0} \u00B0F", inputStr);
                lblOutput.Content = output;
            }
            //C to K
            else if (rbInputScaleC.IsChecked == true && rbOutputScaleK.IsChecked == true)
            {
                double converted = Convert.ToDouble(input) + 273.15;
                string inputStr = string.Format("{0}", converted);
                string output = string.Format("{0} \u00B0K", inputStr);
                lblOutput.Content = output;
            }
            //F to C
            else if (rbInputScaleF.IsChecked == true && rbOutputScaleC.IsChecked == true)
            {
                double converted = Convert.ToDouble(input)* 32 - 32 * 5.0 / 9;
                string inputStr = string.Format("{0}", converted);
                string output = string.Format("{0} \u00B0C", inputStr);
                lblOutput.Content = output;
            }
            //F to F
            else if (rbInputScaleF.IsChecked == true && rbOutputScaleF.IsChecked == true)
            {
                string output = string.Format("{0} \u00B0F", input);
                lblOutput.Content = output;
            }
            //F to K
            else if (rbInputScaleF.IsChecked == true && rbOutputScaleK.IsChecked == true)
            {
                double converted = Convert.ToDouble(input)* 32 - 32 * 5.0 / 9 + 273.15;
                string inputStr = string.Format("{0}", converted);
                string output = string.Format("{0} \u00B0K", inputStr);
                lblOutput.Content = output;
            }
            //K to C
            else if (rbInputScaleK.IsChecked == true && rbOutputScaleC.IsChecked == true)
            {
                double converted = Convert.ToDouble(input)- 273.1;
                string inputStr = string.Format("{0}", converted);
                string output = string.Format("{0} \u00B0C", inputStr);
                lblOutput.Content = output;
            }
            //K to F
            else if (rbInputScaleK.IsChecked == true && rbOutputScaleF.IsChecked == true)
            {
                double converted = Convert.ToDouble(input)- 273.1 * 9.0 / 5 + 32;
                string inputStr = string.Format("{0}", converted);
                string output = string.Format("{0} \u00B0F", inputStr);
                lblOutput.Content = output;
            }
            //K to k
            else if (rbInputScaleK.IsChecked == true && rbOutputScaleK.IsChecked == true)
            {
                string output = string.Format("{0} \u00B0K", input);
                lblOutput.Content = output;
            }
        }
    }
    }

