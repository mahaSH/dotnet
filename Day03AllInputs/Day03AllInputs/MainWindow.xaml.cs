﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void btRegisterMe_Click(object sender, RoutedEventArgs e)
        {
            String name = lbName.Text;
            if (name == null)
            {
                MessageBox.Show("Nmae field should not be empty");
                return;
            }
            //
            string age;
            if (rbBelow18.IsChecked == true)
            {
                age = "below 18";
            }
            else if (rb18To35.IsChecked == true)
            {
                age = "between 18 & 35";
            }
            else
            {
                age = "between 18 & 35";
            }
        
            //
            string pets="";
            if (cbCat.IsChecked == true)
            {
                pets+=",cat";
            }
           if (cbDog.IsChecked == true)
            {
                pets += ",dog";
            }
            if (cbOthers.IsChecked == true)
            {
                pets += ",others";
            }
            //
            string contient= comboContient.Text;
            //
            double temp = sliderTemp.Value;
            //
            string person = string.Format("{ 0};{ 1};{ 2};{ 3};{ 4} ", name, age, pets, contient, temp);
           // MessageBox.Show("followong person added:"+person);
            return;

        }

        private void ComboContient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
