﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbersGenerated = new List<int>();
            Console.WriteLine("Combien de numbres voulez vous crier?");
            int numberOfRandoms = int.Parse(Console.ReadLine());
            Random random = new Random();
            int generated;

            for(int i = 0; i < numberOfRandoms; ++i)
            {
                generated = random.Next(-100, 100);
                numbersGenerated.Add(generated);
            }
            foreach(int i in numbersGenerated)
            {
                if (i <= 0)
                {
                    Console.WriteLine(i);

                }
            }
            Console.WriteLine("Press abny key to continue");
            Console.ReadKey();
        }
    }
}
