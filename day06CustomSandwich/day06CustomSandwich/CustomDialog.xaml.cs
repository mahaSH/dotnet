﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        public CustomDialog()
        {
            InitializeComponent();
        }
         string bread, vegetables, meat;
        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            bread = comboBread.Text;
            //
            List<string> vigitableList = new List<string>();
            if (cbTomato.IsChecked==true)
            {
                vigitableList.Add("tomato");

            }
            if (cbLettuce.IsChecked == true)
            {
                vigitableList.Add("Lettuce");

            }
            if (cbCucumber.IsChecked == true)
            {
                vigitableList.Add("cucumbers");

            }
            vegetables = string.Join(",", vigitableList);
            //
            if (rbChicken.IsChecked == true)
            {
                meat = "chicken";
            }
            else if (rbTurkey.IsChecked == true)
            {
                meat = "Turkey";
            }
            else
            {
                meat = "Ton";
            }
            //
            this.DialogResult = true;
        }
        public string Bread
        {
            get
            {
                return bread;
            }
        }

       

        public string Vegetables
        {
            get
            {
                return vegetables;
            }
        }
        public string Meat
        {
            get
            {
                return meat;
            }
        }
    }
}
