﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        List<Car> mainCarList;
        Car currentCar; // car being edited, null if creating a new one

        public AddEditDialog(List<Car> mainCarList, Car car)
        {
            this.mainCarList = mainCarList;
            currentCar = car;
            InitializeComponent();
            // List<string> enumValsList = Enum.GetValues(typeof(Car.FuelTypeEnum)).Cast<string>().ToList();
            // comboFuelType.ItemsSource = enumValsList;
            comboFuelType.ItemsSource = Enum.GetValues(typeof(Car.FuelTypeEnum));
            comboFuelType.SelectedItem = comboFuelType.Items[0]; // select the first on the list
            if (currentCar != null)
            {
                btSave.Content = "Update";
                // TODO: load car into UI
                tbMakeModel.Text = car.MakeModel;
                sliderEngineSize.Value = car.EngineSizeL;
                comboFuelType.Text = car.FuelType.ToString(); // ?
            }
            else
            {
                btSave.Content = "Create";
            }

        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string makeModel = tbMakeModel.Text;
            double engSizeL = sliderEngineSize.Value;
            Car.FuelTypeEnum fuelType = (Car.FuelTypeEnum)comboFuelType.SelectedItem;
            if (currentCar != null)
            { // update
                try
                {
                    currentCar.MakeModel = makeModel;
                    currentCar.EngineSizeL = engSizeL;
                    currentCar.FuelType = fuelType; // FIXME: check what we get when nothing was selected in combo?
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Invalid input:\n" + ex.Message); // FIXME: better dialog
                    return; // keep dialog open
                }
            }
            else
            { // add new
                try
                {
                    Car car = new Car(makeModel, fuelType, engSizeL);
                    mainCarList.Add(car);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Invalid input:\n" + ex.Message); // FIXME: better dialog
                    return; // keep dialog open
                }
            }
            this.DialogResult = true; // hide dialog
        }
    }
}
