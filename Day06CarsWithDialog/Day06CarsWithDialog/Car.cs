﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06CarsWithDialog
{
    public class Car
    {
        public Car(string makeModel, FuelTypeEnum fuelType, double engineSize)
        {
            MakeModel = makeModel;
            FuelType = fuelType;
            EngineSizeL = engineSize;
        }
        private string _makeModel;
        public string MakeModel
        {// 2-50 characters, no semicolons
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Make model should not be  null");
                }
                if (value.Length < 2 || value.Length > 50 || value.Contains(';')){
                    throw new ArgumentException("Make model should be  2-50 characters, no semicolons");
                }
                _makeModel = value;
                 
            }
            get
            {
                return _makeModel;
            }
        }
       private double _engineSizeL; 
        public double EngineSizeL
        {// 0-20
            set
            {
                if (value< 0 || value > 20)
                {
                    throw new ArgumentException("Engine size should be 0-20");
                }
               _engineSizeL = value;

            }
            get
            {
                return _engineSizeL;
            }
        }
        public FuelTypeEnum FuelType { get; set; }
        public enum FuelTypeEnum {Gasoline,Diesel,Hybrid,Electric,Other }
    }
}
