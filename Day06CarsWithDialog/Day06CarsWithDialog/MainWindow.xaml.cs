﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Car> carsList = new List<Car>();
        public MainWindow()
        {
            InitializeComponent();
            lvCars.ItemsSource = carsList;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void miFileNew_MenuClick(object sender, RoutedEventArgs e)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                // try
                {

                    /* string fileName = saveFileDialog.FileName;
                     File.WriteAllText(fileName, txtEditor.Text);*/
                    using (var writer = new StreamWriter(saveFileDialog.FileName))
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.WriteRecords(carsList);
                        writer.Flush();
                    }

                    // }
                    //catch (IOException ex)
                    // {
                    //     MessageBox.Show(this, "Error reading file:\n" + ex.Message, "File access error", MessageBoxButton.OK);
                }
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(carsList, null);
            dialog.ShowDialog();
            lblCursorPosition.Text = String.Format("There are {0} item(s) on the list", carsList.Count);
            lvCars.Items.Refresh();
        }
        private void miContextEdit_MenuClick(object sender, RoutedEventArgs e)
        {
            Car car = (Car)lvCars.SelectedValue;
            if (car == null)
            {
                return;
            }
            AddEditDialog addEditDialog = new AddEditDialog(carsList, car);
            lblCursorPosition.Text = String.Format("There are {0} item(s) on the list", carsList.Count);
            lvCars.Items.Refresh();
        }

        private void miContextDelete_MenuClick(object sender, RoutedEventArgs e)
        {
            Car car = (Car)lvCars.SelectedValue;
            if (car == null)
            {
                return;
            }
            // TODO: show confirmation dialog
            // if user confirmed remove item from carsList and refresh lvCars
        }
        private void lvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            miContextEdit_MenuClick(sender, e);
        }
    }
}

