﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    public class SocietyDBContext1 : DbContext
    {
        public SocietyDBContext1()
        {
            //this.Configuration.LazyLoadingEnabled = false;   // For LazyLoading
            //this.Configuration.ProxyCreationEnabled = true; // For LazyLoading
        }
        virtual public DbSet<Owner> Owners { get; set; }
        virtual public DbSet<Car> Cars { get; set; }
    }
}
