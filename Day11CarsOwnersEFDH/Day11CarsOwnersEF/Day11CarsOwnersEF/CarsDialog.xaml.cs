﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        Owner crtOwner;
        SocietyDBContext1 ctx;
        public CarsDialog(Owner selOwner,SocietyDBContext1 ctx)
        {
            InitializeComponent();
            this.ctx = ctx;
            crtOwner = selOwner;

            lvCars.ItemsSource = (from car in ctx.Cars where car.Owner.Id == crtOwner.Id select car).ToList<Car>();

            txtId.Text = "-";
            txtOwnerName.Text = crtOwner.Name;
            tbMakeModel.Text = "";

            btUpdate.IsEnabled = false;
            btDelete.IsEnabled = false;
        }

        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Car selCar = (Car)lvCars.SelectedItem;
            if(selCar == null)
            {
                txtId.Text = "-";
                txtOwnerName.Text = crtOwner.Name;
                tbMakeModel.Text = "";

                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
                return;
            }

            txtId.Text = selCar.Id.ToString();
            txtOwnerName.Text = crtOwner.Name;
            tbMakeModel.Text = selCar.MakeModel;

            btUpdate.IsEnabled = true;
            btDelete.IsEnabled = true;

        }

        private void btDone_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string makeModel = tbMakeModel.Text;
            if(makeModel.Length >100 || makeModel == "")
            {
                MessageBox.Show(this,
                    "MakeModel has to be 1~100 characters",
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            ctx.Cars.Add(new Car() { MakeModel = makeModel, Owner = crtOwner });
            ctx.SaveChanges();
            lvCars.ItemsSource = (from car in ctx.Cars where car.Owner.Id == crtOwner.Id select car).ToList<Car>();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            Car selCar = (Car)lvCars.SelectedItem;
            if(selCar == null)
            {
                MessageBox.Show(this,
                    "Choose one of car in the list.",
                    "No Car Selected",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            string makeModel = tbMakeModel.Text;
            if (makeModel.Length > 100 || makeModel == "")
            {
                MessageBox.Show(this,
                    "MakeModel has to be 1~100 characters",
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            selCar.MakeModel = makeModel;
            ctx.SaveChanges();
            lvCars.ItemsSource = (from car in ctx.Cars where car.Owner.Id == crtOwner.Id select car).ToList<Car>();
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            Car selCar = (Car)lvCars.SelectedItem;
            if (selCar == null)
            {
                MessageBox.Show(this,
                    "Choose one of car in the list.",
                    "No Car Selected",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            ctx.Cars.Remove(selCar);
            ctx.SaveChanges();
            lvCars.ItemsSource = (from car in ctx.Cars where car.Owner.Id == crtOwner.Id select car).ToList<Car>();
        }
    }
}
