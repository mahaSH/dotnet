﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Program
    {
        static List<Person> peopleList = new List<Person>();
        class Person
        {

            //
            protected Person()
            {

            }
            public Person(string name, int age)
            {
                Name = name;
                Age = age;
            }
            //
            public string _name;
            public string Name
            {// 1-50 characters, no semicolons
                set
                {
                    if (value.Length < 0 || value.Length > 50 || value.Contains(";"))
                    {
                        throw new InvalidOperationException("name's length should be greater than 0 and less than 50 with no semicolin");
                    }
                    _name = value;
                }
                get
                {
                    return _name;
                }
            }
            public int _age; // 0-150
            public int Age
            {
                set
                {
                    if (value > 150 || value < 0)
                    {
                        throw new InvalidOperationException("age should be greater than 0 and less than 150 ");
                    }
                    _age = value;
                }
                get
                {
                    return _age;
                }
            }
                   public override string ToString()
            {
                return string.Format("Person: {0} is {1} y/o", Name, Age);
            }

        

        public Person(string dataLine) {
                
                string[] line = dataLine.Split(';');
                if (line.Length != 3)
                {
                    Console.WriteLine("Invalid line:");
                }
                else
                {
                    string name= line[1];
                    Name = name;
                    Console.WriteLine(line[1]);
                    int age;
                    if (!int.TryParse(line[2], out age))
                    {
                        Console.WriteLine("Invalid number");
                    }

                    Age = age;
                }
            }


        }

        class Teacher : Person
        {
            public string _subject;
            public string Subject
            {// 1-50 characters, no semicolons
                set
                {
                    if (value.Length < 0 || value.Length > 50 || value.Contains(';'))
                    {
                        throw new InvalidOperationException(" subject name's length should be greater than 0 and less than 50 with no semicolin");
                    }
                    _subject = value;
                }
                get
                {
                    return _subject;
                }
            }
            public int _yearsOfExperience;
            public int YearsOfExperience
            {// 0-100
                set
                {
                    if (value > 100 || value < 0)
                    {
                        throw new InvalidOperationException("yearsOfExperience should be greater than 0 and less than 100");
                    }
                    _yearsOfExperience = value;
                }
                get
                {
                    return _yearsOfExperience;
                }
            }
            public override string ToString()
            {
                return string.Format("Teachet:{0} is {1}y/o .Teaches {2} for {3} years", Name, Age,Subject,YearsOfExperience);
            }
            public Teacher(string dataLine) {
                Teacher T;
                string[] line = dataLine.Split(';');
                if (line.Length != 5)
                {
                    Console.WriteLine("Invalid line:");
                }
                else
                {
                    string name = line[1];

                    Name = name;
                    Subject = line[3];
                    int yoe;
                    int age;
                    if (!int.TryParse(line[2], out age))
                    {
                        throw new InvalidOperationException(" GPA shuld be between 0 and 4.3");
                       

                    }
                    Age = age;
                    string subject = line[3];
                    Subject = subject;
                    if (!int.TryParse(line[4], out yoe))
                    {
                        throw new InvalidOperationException(" GPA shuld be between 0 and 4.3");
                    }
                    
                    YearsOfExperience = yoe;

                }
            }

            public Teacher(string name, int age, string subject, int yoe) {
                Name = name;
                Age = age;
                Subject = subject;
                YearsOfExperience = yoe;
            }
        }

        class Student : Person
        {
            public Student(string name, int age, string program, double gpa)
            {
                Name = name;
                Age = age;
                Program = program;
                GPA = gpa;
            }
            public string _program;
            public string Program
            {// 1-50 characters, no semicolons
                set
                {
                    if (value.Length < 0 || value.Length > 50 || value.Contains(';'))
                    {
                        throw new InvalidOperationException(" program name's length should be greater than 0 and less than 50 with no semicolin");
                    }
                    _program = value;
                }
                get
                {
                  return  _program ;

                }
            }
            public double _gpa; // 0-4.3
            public double GPA
            {
                set
                {
                    if (value < 0 || value > 4.3)
                    {
                        throw new InvalidOperationException(" GPA shuld be between 0 and 4.3");
                    }
                }
                get
                {
                    return _gpa;
                }
            }
            public override string ToString()
            {
                return string.Format("Student:{0} is {1} y/o studies {2} .the GPA is {3}\n", Name, Age,Program,GPA);
            }
            public Student(string dataLine) {
                int age;
                double gpa;
                string name;
                string program;
                string[] line = dataLine.Split(';');
                if (line.Length != 5)
                {
                    throw new InvalidOperationException(" GPA shuld be between 0 and 4.3");
                }
               name = line[1];
                Name = name;

                if (!int.TryParse(line[2], out age))
                {
                    throw new InvalidOperationException(" GPA shuld be between 0 and 4.3");
                }
                Age = age;
                program = line[3];
                Program = program;
                if (!double.TryParse(line[2], out gpa))
                {
                    Console.WriteLine("Invalid number");
                }
                GPA = gpa;
            }
           
        }
    
        static void Main(string[] args)
        {
            Console.WriteLine(@"Where would you like to log setters errors?
1-screen only
2-screen and file
3-do not log
Your choice:");
            int logChoise = int.Parse(Console.ReadLine());

            try
            {
                var dataFromFile = File.ReadAllLines(@"..\..\people.txt");
                string[] tokens = dataFromFile.ToArray();//CONVERT THE WHOLE FILE TO AN ARRAY OF LINES(TOKENS
                for (int i = 0; i < tokens.Length; ++i)
                {
                    string line = tokens[i];//KEEP THE LINE A SIDE
                    string[] splitedLine = tokens[i].Split(';');//SPLIT THE LINE BY ";" 
                    if (splitedLine.Length < 3)
                    {
                        Console.WriteLine("Invalid line:", tokens[i]);
                        continue;
                    }
                    


                        switch (splitedLine[0])
                        {
                            case ("Person"):
                                Person p = new Person(line);
                                peopleList.Add(p);
                                break;
                            case ("Teacher"):
                                Teacher t = new Teacher(line);
                                peopleList.Add(t);
                                break;
                            case ("Student"):
                                Student S = new Student(line);
                                peopleList.Add(S);
                                break;
                            default:
                                Console.WriteLine("unsuitable line");
                                break;
                        }
                    }
                
            }

            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
 foreach(Person p in peopleList)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine("people sorted\n");
            var peopleOrderedByName = from p in peopleList orderby p.Name select p;
            Console.WriteLine("\n general list:\n ****************");
            foreach (Person p in peopleOrderedByName)

            {
                Console.WriteLine(p);
            }
            Console.WriteLine("\n Teachers list:\n ****************");
            foreach (Person p in peopleOrderedByName)

            {
                if(p is Teacher)
                Console.WriteLine(p);
            }
            Console.WriteLine("\n Persons list:\n ****************");
            foreach (Person p in peopleOrderedByName)

            {
                if (p.GetType()==typeof(Person))
                    Console.WriteLine(p);
            }
            Console.WriteLine("press any key to continue\n");
            Console.ReadKey();
        }
    }
}
