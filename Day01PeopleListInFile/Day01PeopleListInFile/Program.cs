﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    public class Person//:Object
    {
        public Person(string name, int age, string city)
        {
            Name = name;//CALLES SETTER
            Age = age;//CALLES SETTER
            City = city;//CALLES SETTER
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentException("Name length must be 2 characters or more");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 250)
                {
                    throw new ArgumentException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }
        private string _city;
        public string City // City 2-100 characters long, not containing semicolons
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("City length must be 2 characters or more with no semicolin");
                }
                _city = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Name: {0} is {1}y/o lives in :{2}\n", Name, Age, City);
        }

    }

    class Program
    {
        static List<Person> peopleList = new List<Person>();
        static void AddPersonInfo()
        {
            Console.WriteLine(@"Adding a person.
Enter name:");
            String name = Console.ReadLine();
            Console.WriteLine("Enter age:");
            int age;
            string ageStr = Console.ReadLine();
            if (!int.TryParse(ageStr, out age))
            {
                Console.WriteLine("Invalid number");
            }
            age= int.Parse(ageStr); 
            Console.WriteLine("Enter city");
            String city = Console.ReadLine();
            Person p = new Person(name, age, city);
            peopleList.Add(p);
            Console.WriteLine("Person added \n \n");
            
        }
        static void ListAllPersonsInfo()
        {
            Console.WriteLine("Listing all persons\n");
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p);
            }
        }
        static void FindPersonByName()
        {
            Console.WriteLine("Enter partial person name:");
            string part = Console.ReadLine();
            Boolean found = false;
            foreach (Person p in peopleList)
            {
                if (p.Name.Contains(part) || p.City.Contains(part))
                {
                    Console.WriteLine("Matches found:\n");
                    Console.WriteLine(p);
                    found = true;
                }
                
            }
            if(found==false)
                    Console.WriteLine("No matches found \n");
        }
        static void FindPersonYoungerThan()
        {
            Console.WriteLine("Enter maximum age:");
            int age =int.Parse (Console.ReadLine());
            foreach (Person p in peopleList)
            {
                if (p.Age < age)
                {
                    Console.WriteLine(p);
                }

            }
        }
		static void LoadDataFromFile()
		{
            Person p;
            try
            {
                var dataFromFile = File.ReadAllLines(@"..\..\people.txt");
                string[] tokens = dataFromFile.ToArray();

                for (int i = 0; i < tokens.Length; ++i)
                {

                    string[] line = tokens[i].Split(';');
                    if (line.Length != 3)
                    {
                        Console.WriteLine("Invalid line:", i);
                        continue;
                    }
                    else
                        p = new Person(line[0], int.Parse(line[1]), line[2]);
                    peopleList.Add(p);
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }
		static int getMenuChoice(){
		 while (true)
            {
                Console.Write(
    @"1. Add person info
2. List persons info
3. Find a person by name
4. Find all persons younger than age
0. Exit
Enter your choice: ");
                int choice;
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
                if (choice< 0 || choice> 4)
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
                return choice;
            }
        }
		static void SaveDataToFile()
		{
			try
			{
				List<String> linesList = new List<String>();
				foreach (Person p in peopleList)
				{
					linesList.Add(String.Format("{0};{1};{2}", p.Name, p.Age, p.City));
				}
				File.WriteAllLines(@"..\..\people.txt", linesList); // ex
			}
			catch (IOException ex)
			{
				Console.WriteLine("Error writing file: " + ex.Message);
			}
			catch (SystemException ex)
			{
				Console.WriteLine("Error writing file: " + ex.Message);
			}

			/* // Useful when handling 3 or more exceptions the same way
             catch (Exception ex)
            {
                if (ex is IOException || ex is SystemException)
                {
                    Console.WriteLine("Error writing file: " + ex.Message);
                }
                else
                {
                    throw ex; // continue propagation
                }
            } */
		}
		static void Main(string[] args)
		{
			LoadDataFromFile();
			while (true)
			{
				int choice = getMenuChoice();
				switch (choice)
				{
					case 1:
						AddPersonInfo();
						break;
					case 2:
						ListAllPersonsInfo();
						break;
					case 3:
						FindPersonByName();
						break;
					case 4:
						FindPersonYoungerThan();
						break;
					case 0:
						SaveDataToFile();
						Console.WriteLine("Data saved. Exiting.");
						return; // terminate the program
					default:
						Console.WriteLine("Internal error: invalid control flow in menu");
						break;
				}
			}
		}
	}
}
